time 类
    getDate
        /**
       * [getDate 格式化日期格式]
       * @param  {String} formart [格式化日期的格式 默认格式 ‘yyyy-MM-dd hh:mm:ss’]
       * @param  {String} param   [被格式化的日期]
       * @return {[type]}         [返回格式化后的日期]
       */
       time.getDate()
       time.getDate('yyyy/MM/dd')
       time.getDate('yyyy/MM/dd', 1538981665992)
       time.getDate('yyyy-MM-dd', 1538981665992)
       time.getDate('', 1538981665992)
