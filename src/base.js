/**
 * 定义基础公共方法
 */
class Base{
  /**
   * [getQueryObject 获取地址栏数据]
   * @param  {[type]} url [地址栏字符串参数]
   * @return {[type]}     [返回解析后的数据]
   */
  getQueryObject(url) {
    let u = url ? window.location.href : url
    let search = u.substring(u.lastIndexOf('?') + 1)
    let obj = {}
    search.replace(/([^?&=]+)=([^?&=]*)/g, (rs, $1, $2) => {
      let name = decodeURIComponent($1)
      let val = decodeURIComponent($2)
      val = String(val)
      obj[name] = val
      return rs
    })
    return obj
  }
}
export default new Base()
