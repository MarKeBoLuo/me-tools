/**
 * 时间处理函数
 */
class Time{
  _new(param) {
    return param ? new Date(param) : new Date()
  }
  _dateFormat(fmt, date) {
    let o = {
      'M+' : date.getMonth()+1,
      'd+' : date.getDate(),
      'h+' : date.getHours(),
      'm+' : date.getMinutes(),
      's+' : date.getSeconds(),
      'q+' : Math.floor((date.getMonth() + 3) / 3),
      'S'  : date.getMilliseconds()
    }
    if (new RegExp('(y+)').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear())).substr(4 - RegExp.$1.length)
    }
    for (var k in o) {
      if (new RegExp('('+ k +')').test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ?
          (o[k]) : (('00'+ o[k]).substr(('' + o[k]).length)))
      }
    }
    return fmt
  }
  /**
   * [getDate 格式化日期格式]
   * @param  {String} formart [格式化日期的格式]
   * @param  {String} param   [被格式化的日期]
   * @return {[type]}         [返回格式化后的日期]
   */
  getDate(formart, param = '') {
    let time = this._new(param)
    return this._dateFormat(formart ? formart : 'yyyy-MM-dd hh:mm:ss', time)
  }
}
export default new Time()
