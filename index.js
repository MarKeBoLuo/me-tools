/**
 * author: markkill 形参思 深渊血河 MarkBoLuo
 * Created by markkill on 2018/9/29.
 */
import base from './src/base.js'
import time from './src/time.js'
export default{
  base,
  time
}
