base类
    getQueryObject
        /**
       * [getQueryObject 获取地址栏数据]
       * @param  {[type]} url [地址栏字符串参数]
       * @return {[type]}     [返回解析后的数据]
       */
        base.getQueryObject() // 直接解析当前浏览器地址的参数
        base.getQueryObject(url) // 解析url返回对象格式的键值对
